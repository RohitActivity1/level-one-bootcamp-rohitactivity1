//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input1()
{
   int x;
   printf("Enter number:");
   scanf("%d",&x);
   return x;
}

int input2()
{
  int x;
  printf("Enter number:");
  scanf("%d",&x);
  return x;
}
int add(int a, int b)
{
  return (a+b);
}

void print(int sum)
{ 
 printf("sum = %d",sum);
}
int main()
{
    int a=input1();
    int b=input2();
    int sum=add(a,b);
    print(sum);
 
     return 0;
}