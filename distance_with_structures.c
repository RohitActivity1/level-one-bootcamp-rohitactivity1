//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>

struct Point {
    int x, y;
};


int Distance(struct Point a, struct Point b)
{
    int distance;
    distance = sqrt((a.x - b.x) * (a.x - b.x) + (a.y-b.y) *(a.y-b.y));
    return distance;
}



int main()
{
    struct Point a, b;
    printf("Enter x1: ");
    scanf("%d", &a.x);
     printf("Enter y1: ");
    scanf("%d", &a.y);
    printf("Enter x2: ");
    scanf("%d", &b.x);
     printf("Enter y2: ");
    scanf("%d", &b.y);
   
     printf("Distance between (%d,%d) and (%d,%d) is %d",a.x,a.y,b.x,b.y,Distance(a,b));



    return 0;
}