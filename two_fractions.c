//WAP to find the sum of two fractions.
#include<stdio.h>
struct fraction
{
   int numerator;
   int denominator;
};
typedef struct fraction Fraction;

Fraction input()
{
    Fraction f;
    printf("\n Enter Numerator: ");
    scanf("%d", &f.numerator);
    printf( "\n Enter denominator: ");
    scanf("%d",&f.denominator);
    return f;
}
int hcf ( int a, int b)
{
int i, gcd=1;
for (i=2; i<=a && i<=b;i++)
{
    if(a%i==0 && b%i==0)
    gcd=i;
}
return gcd;
}

Fraction sum(Fraction a, Fraction b)
{
int gcd;
       Fraction temp;
       temp.numerator=((a.numerator*b.denominator)+(b.numerator*a.denominator));
       temp.denominator=(a.denominator*b.denominator);
       gcd = hcf(temp.numerator,temp.denominator);
       temp.numerator=temp.numerator/gcd;
       temp.denominator=temp.denominator/gcd;
       return temp;
}
void result(Fraction a, Fraction b,Fraction sum)
{
printf("The sum of %d/%d+%d/%d is %d/%d", a.numerator,a.denominator, b.numerator,b.denominator,sum.numerator,sum.denominator);
}
int main()
{
Fraction a,b,res;
printf("Enter the numerator and denominator of first fraction: ");
a=input();
printf("Enter the numerator and denominator of second fraction:");
b=input();
res=sum(a,b);
result(a,b,res);
return 0;
}
